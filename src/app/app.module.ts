import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes, Router } from '@angular/router'


import { AppComponent } from './app.component';
import { HeaderTitleComponent } from './header-title/header-title.component';
import { ListSectionComponent } from './list-section/list-section.component';
import { ParticlesComponent } from './particles/particles.component';
import { CarFormComponent } from './car-form/car-form.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ParkingFormComponent } from './parking-form/parking-form.component';
import { CarShowComponent } from './car-show/car-show.component';
import { ParkingShowComponent } from './parking-show/parking-show.component';

const routes: Routes = [
  { 'path' : 'cars', component: CarShowComponent},
  { 'path' : 'places', component : ParkingShowComponent},
  { 'path' : 'place/edit/:id', component : ParkingFormComponent},
  { 'path' : 'place/new', component : ParkingFormComponent},
  { 'path' : 'car/edit/:id', component : CarFormComponent},
  { 'path' : 'car/new', component : CarFormComponent},
]

@NgModule({
  declarations: [
    AppComponent,
    HeaderTitleComponent,
    ListSectionComponent,
    ParticlesComponent,
    CarFormComponent,
    ParkingFormComponent,
    CarShowComponent,
    ParkingShowComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    NgbModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
