import { Component, OnInit } from '@angular/core';
import { environment } from '../../environments/environment';

import { dataOperations as JexiaDataOperations, jexiaClient } from "jexia-sdk-js/browser";


const dataModule = JexiaDataOperations();
jexiaClient().init(environment.jexia, dataModule);
const voiture = dataModule.dataset("voiture");

@Component({
  selector: 'app-car-show',
  templateUrl: './car-show.component.html',
  styleUrls: ['./car-show.component.css']
})
export class CarShowComponent implements OnInit {

  cars = [];
  selectQuery = voiture
      .select();

  constructor() { }

  ngOnInit(): void {
    this.refreshSelect();
  }

  
  delete(id){
    voiture
    .delete()
    .where(field => field("id").isEqualTo(id))
    .subscribe(records => {
      console.log("Supression:", records)
      this.refreshSelect();
    }, error => {
      console.error("Erreur:", error)
    });
  }

  refreshSelect(){
    this.selectQuery.subscribe(records => {
      console.log("Voitures:", records)
      this.cars = records;
    },
    error => {
      console.error("Erreur:", error)
    });
  }

}
