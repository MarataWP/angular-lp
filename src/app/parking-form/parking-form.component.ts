import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { environment } from '../../environments/environment';

import { dataOperations as JexiaDataOperations, jexiaClient } from "jexia-sdk-js/browser";

import { ActivatedRoute, Router } from '@angular/router';

const dataModule = JexiaDataOperations();
jexiaClient().init(environment.jexia, dataModule);
const parking = dataModule.dataset("placeParking");

@Component({
  selector: 'app-parking-form',
  templateUrl: './parking-form.component.html',
  styleUrls: ['./parking-form.component.css']
})
export class ParkingFormComponent implements OnInit {

  parkingForm: FormGroup;
  submitted = false;
  id = null;
  place = null;

  constructor(private router: Router, private route: ActivatedRoute, private fb: FormBuilder) { }

  ngOnInit(): void {
    if (this.route.snapshot.paramMap.get('id')) {
      this.id = this.route.snapshot.paramMap.get('id');
      parking
      .select()
      .where(field => field("id").isEqualTo(this.id))
      .subscribe(records => {
        console.log("Place sélectionné:", records)
        if (records.length == 1){
          this.place = records[0];
          this.parkingForm = this.fb.group({
            numero: [records[0].numero,Validators.required],
            couverte: [records[0].couverte, Validators.required],
            longueur: [records[0].longueur, Validators.required],
            largeur: [records[0].largeur, Validators.required],
          });
        }
      },
      error => {
        console.error("Erreur :", error)
      });
    }

    this.parkingForm = this.fb.group({
      numero: ['',Validators.required],
      couverte: [false, Validators.required],
      longueur: ['', Validators.required],
      largeur: ['', Validators.required],
    });
  }

  get f() { 
    return this.parkingForm.controls; 
  }

  submit() {
    this.submitted = true;
    if (this.parkingForm.invalid) {
      return;
    }

    var data = {
      "numero": this.parkingForm.get('numero').value,
      "couverte" : this.parkingForm.get('couverte').value,
      "longueur" : this.parkingForm.get('longueur').value,
      "largeur" : this.parkingForm.get('largeur').value
    }
    if (this.place == null){
      parking
      .insert(data)
      .subscribe(records => {
        console.log("Nouvelle place:", records);
        this.router.navigate(['places']);
      }, 
      error => {
        console.error("Erreur:", error)
      });
    }else{
      parking
      .update(data)
      .where(field => field("id").isEqualTo(this.place.id))
      .subscribe(records => {
        console.log("Place modifiée:", records);
        this.router.navigate(['places']);
      }, error => {
        console.error("Erreur:", error)
      });
    }
  }

}
