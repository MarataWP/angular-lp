import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { environment } from '../../environments/environment';

import { dataOperations as JexiaDataOperations, jexiaClient } from "jexia-sdk-js/browser";

import { ActivatedRoute, Router } from '@angular/router';

const dataModule = JexiaDataOperations();
jexiaClient().init(environment.jexia, dataModule);
const voiture = dataModule.dataset("voiture");

@Component({
  selector: 'app-car-form',
  templateUrl: './car-form.component.html',
  styleUrls: ['./car-form.component.css']
})
export class CarFormComponent implements OnInit {

  carForm: FormGroup;
  submitted = false;
  id = null;
  voiture = null;

  constructor(private router: Router, private route: ActivatedRoute, private fb: FormBuilder) { }

  ngOnInit(): void {
    if (this.route.snapshot.paramMap.get('id')) {
      this.id = this.route.snapshot.paramMap.get('id');
      var selectQuery = voiture
      .select()
      .where(field => field("id").isEqualTo(this.id))
      ;

    selectQuery.subscribe(records => {
        console.log("Voiture sélectionné:", records)
        if (records.length == 1){
          this.voiture = records[0];
          this.carForm = this.fb.group({
            nbPlaces: [records[0].nbPlaces,Validators.required],
            modele: [records[0].modele, Validators.required],
            motorisation: [records[0].motorisation],
            miseEnService: [records[0].miseEnService, Validators.required],
            kilometrage: [records[0].kilometrage],
            immatriculation: [records[0].immatriculation],
          });
        }
      },
      error => {
        console.error("Erreur :", error)
      });
    }
    this.carForm = this.fb.group({
      nbPlaces: ['',Validators.required],
      modele: ['', Validators.required],
      motorisation: [''],
      miseEnService: ['', Validators.required],
      kilometrage: [''],
      immatriculation: ['', Validators.required],
    });
  }

  get f() { 
    return this.carForm.controls; 
  }

  submit() {
    this.submitted = true;
    if (this.carForm.invalid) {
      return;
    }

    var data = {
      "nbPlaces": this.carForm.get('nbPlaces').value,
      "modele" : this.carForm.get('modele').value,
      "motorisation" : this.carForm.get('motorisation').value,
      "miseEnService" : this.carForm.get('miseEnService').value,
      "kilometrage" : this.carForm.get('kilometrage').value,
      "immatriculation": this.carForm.get('immatriculation').value,
    };
    if (this.voiture == null){
      var insertQuery = voiture.insert(data);

      insertQuery.subscribe(records => {
        console.log("Nouvelle voiture:", records);
        this.router.navigate(['cars']);
      }, 
      error => {
        console.error("Erreur:", error)
      // if something goes wrong, you can see the error info here
      });
    }else{
      voiture
      .update(data)
      .where(field => field("id").isEqualTo(this.voiture.id))
      .subscribe(records => {
        console.log("Voiture modifiée:", records);
        this.router.navigate(['cars']);
      }, error => {
        console.error("Erreur:", error)
      });
    }
  }
}
