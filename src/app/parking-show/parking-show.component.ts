import { Component, OnInit } from '@angular/core';
import { environment } from '../../environments/environment';

import { dataOperations as JexiaDataOperations, jexiaClient } from "jexia-sdk-js/browser";


const dataModule = JexiaDataOperations();
jexiaClient().init(environment.jexia, dataModule);
const place = dataModule.dataset("placeParking");

@Component({
  selector: 'app-parking-show',
  templateUrl: './parking-show.component.html',
  styleUrls: ['./parking-show.component.css']
})
export class ParkingShowComponent implements OnInit {

  places = [];
  selectQuery = place
      .select();

  constructor() { }

  ngOnInit(): void {
    this.refreshSelect();
  }

  delete(id){
    place
    .delete()
    .where(field => field("id").isEqualTo(id))
    .subscribe(records => {
      console.log("Supression:", records)
      this.refreshSelect();
    }, error => {
      console.error("Erreur:", error)
    });
  }

  refreshSelect(){
    this.selectQuery.subscribe(records => {
      console.log("Places:", records)
      this.places = records;
    },
    error => {
      console.error("Erreur:", error)
    });
  }

}
