# Projet Angular - Jexia [Gillot Yann & Darras Benjamin]

Comme demandé, nous avons réalisé un projet angular s'appuyant sur Jexia pour produire un CRUD

## Lancement
Voici les deux commandes à exécuter avant le lancement :
```
npm install
ng serve
```

## Configuration

Dans les fichiers "enivronement.ts" et "environement.prod.ts", se trouvent la configuration de la connection à Jexia.


## Datasets

L'outil se compose de deux datasets :
 - Voiture (Nombre de places/Modele/Motorisation/Mise En Service/Kilometrage/Immatriculation)
 - Place de Parking (Numéro/Longueur/Largeur/Couverte?)

```
Certains champs sont requis, d'autres sont facultatifs
```


## Interface
Nous avons choisi une simple navbar qui permet de naviguer entre les différents asset. Une fois sur un asset, vous tomberez sur un tableau des entités déjà créés et vous pouvez soit ajouter en ajouter, soit en modifier, soit en supprimer.

## Style
Nous tenons à préciser que tout le site est stylisé avec bootstrap et que l'outil est 100% responsive.